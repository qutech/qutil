.. qutil documentation master file, created by
   sphinx-quickstart on Wed Jun  8 12:00:57 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Welcome to qutil's documentation!
=================================

.. include:: readme_link.md
   :parser: myst_parser.sphinx_

qutil API Documentation
-----------------------

.. autosummary::
   :toctree: _autogen
   :recursive:

   qutil


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
