# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))

# -- Project information -----------------------------------------------------
from qutil import __version__

project = 'qutil'
copyright = f"{__version__.split('.')[0]}, Quantum Technology Group, RWTH Aachen University"
author = 'Quantum Technology Group, RWTH Aachen University'

# The full version, including alpha/beta/rc tags
release = __version__


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autosummary',
    'sphinx.ext.intersphinx',
    'sphinx.ext.napoleon',
    'sphinx.ext.todo',
    'myst_parser',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'pydata_sphinx_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
html_logo = "_static/logo.png"
html_favicon = "_static/favicon.png"

# https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html
autodoc_mock_imports = [
    'serial',
    'tqdm',
    'ezdxf',
    'qcodes',
    'matlab',
    'xarray',
    'hdf5storage',
    'fastz',
    'tifffile',
    'moviepy',
]
autodoc_typehints = 'description'
# The following aliases override the unrolled aliases that the keys of
# the dictionary evaluate to at runtime.
# Requires from future import __annotations__ import in files that use
# the aliases even on py312
_numpy_typing_objs = {'ArrayLike', 'DTypeLike', 'NBitBase', 'NDArray'}
autodoc_type_aliases = {
    # from numpy.typing import ...
    **{f'{obj}': f'numpy.typing.{obj}' for obj in _numpy_typing_objs},
    # import numpy.typing as npt
    ** {f'npt.{obj}': f'numpy.typing.{obj}' for obj in _numpy_typing_objs}
}

autosummary_generate = True

# https://www.sphinx-doc.org/en/master/usage/extensions/todo.html
todo_include_todos = True

intersphinx_mapping = {
    'python': ('https://docs.python.org/3/', None),
    'numpy': ('https://numpy.org/doc/stable/', None),
    'scipy': ('https://docs.scipy.org/doc/scipy/', None),
    'matplotlib': ('https://matplotlib.org/stable/', None),
    'qutip': ('https://qutip.readthedocs.io/en/latest/', None),
    'pandas': ('https://pandas.pydata.org/docs/', None),
    'qopt': ('https://qopt.readthedocs.io/en/latest/', None),
}
