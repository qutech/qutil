"""Mock spectrometer import for pytest."""
import os
import sys
from types import ModuleType
from unittest.mock import Mock

import pytest

sys.modules['python_spectrometer'] = ModuleType('python_spectrometer')
sys.modules['python_spectrometer'].Spectrometer = Mock()

sys.modules['python_spectrometer.daq'] = ModuleType('python_spectrometer.daq')

LAUNCH_TIMEOUT = 30
IN_GITLAB_CI = os.environ.get('GITLAB_CI', 'False').capitalize() == 'True'


# https://docs.pytest.org/en/latest/example/simple.html#control-skipping-of-tests-according-to-command-line-option
def pytest_addoption(parser):
    """Run all tests also using multiprocessing.

    If false, only the first test with in_process=True will be run.

    By default, this is enabled if the test is running on CI and
    disabled else (Windows takes a looong time to start processes).
    """
    parser.addoption(
        "--runall-inprocess",
        action="store_true",
        default=IN_GITLAB_CI,
        help="run all tests where in_proccess=True"
    )


@pytest.fixture(scope='session')
def launch_timeout():
    return LAUNCH_TIMEOUT


@pytest.fixture(scope='session')
def in_gitlab_ci():
    return IN_GITLAB_CI
