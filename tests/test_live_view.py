import time
from collections.abc import Callable

import numpy as np
import pytest
from matplotlib import pyplot as plt
from matplotlib.backend_bases import CloseEvent, DrawEvent

from qutil import functools, misc
from qutil.plotting import is_using_mpl_gui_backend
from qutil.plotting.live_view import (
    iterable_data_source_factory,
    BatchedLiveView1D,
    BatchedLiveView2D,
    IncrementalLiveView1D,
    IncrementalLiveView2D,
    LiveViewT,
    DataSource
)


def wait_for_interval(interval, t0):
    dt = time.perf_counter() - t0
    if interval > dt:
        time.sleep(interval - dt)
    return time.perf_counter()


def statement_timed_out(stmt: Callable[[], bool], timeout) -> bool:
    try:
        with misc.timeout(timeout, raise_exc=True) as exceeded:
            while not stmt() and not exceeded:
                time.sleep(10e-3)
    except TimeoutError:
        return True
    else:
        return False


def get_constructor(cls, in_process: bool, in_gitlab_ci: bool):
    # Forking can lead to unforeseeable issues.
    # https://docs.python.org/3/library/multiprocessing.html#multiprocessing-start-methods
    if not in_process:
        return cls
    else:
        return functools.partial(getattr(cls, 'in_process'),
                                 context_method='spawn',
                                 backend='agg' if in_gitlab_ci else None)


def start_animation(fig):
    # Need to manually trigger events in headless mode. Else does nothing.
    if not is_using_mpl_gui_backend(fig):
        # Start the animation
        fig.canvas.callbacks.process(
            'draw_event', DrawEvent('draw_event', fig.canvas, fig.canvas.get_renderer())
        )


def advance_frames(view: LiveViewT, n_frames: int):
    # Need to manually trigger events in headless mode. Else does nothing.
    if not is_using_mpl_gui_backend(view.fig):
        # Start the animation
        for _ in range(n_frames):
            for fun, args, kwargs in view.animation.event_source.callbacks:
                fun(*args, **kwargs)


def close_figure(fig):
    if not is_using_mpl_gui_backend(fig):
        # Need to manually trigger the close event because the event loop isn't running
        fig.canvas.callbacks.process('close_event', CloseEvent('close_event', fig.canvas))

    # remove figure from registry
    plt.close(fig)


def put_data_batched_1d(rng, n_lines):
    def produce_data(interval=1e-1):
        t0 = t1 = time.perf_counter()
        while True:
            t1 = wait_for_interval(interval, t1)
            x = 1 + (t1 - t0) * 100
            yield np.arange(1024), x * rng.standard_normal((1024, n_lines))

    return iterable_data_source_factory(produce_data)


def put_data_incremental_1d(rng, n_lines):
    def produce_data(interval=1e-3):
        t0 = t1 = time.perf_counter()
        while True:
            t1 = wait_for_interval(interval, t1)
            x = 1 + (t1 - t0) * 100
            yield np.arange(128), x * rng.standard_normal(n_lines)

    return iterable_data_source_factory(produce_data)


def put_data_batched_2d(rng):
    def produce_data(interval=1e-1):
        x = np.linspace(3.2, 5.7, 1920)
        y = np.linspace(-1.2, 4, 1080)
        t0 = t1 = time.perf_counter()
        while True:
            t1 = wait_for_interval(interval, t1)
            c = 1 + (t1 - t0) * 100
            yield x, y, c * rng.standard_normal((1080, 1920))

    return iterable_data_source_factory(produce_data)


def put_data_incremental_2d(rng):
    def produce_data(interval=1e-3):
        x = np.linspace(3.2, 5.7, 200)
        y = np.linspace(1500, 1900, 20)
        t0 = t1 = time.perf_counter()
        while True:
            t1 = wait_for_interval(interval, t1)
            c = 1 + (t1 - t0) * 100
            yield x, y, c * rng.standard_normal(200)

    return iterable_data_source_factory(produce_data)


@pytest.fixture(scope='module')
def rng():
    return np.random.default_rng()


@pytest.fixture(scope='session')
def in_process_tracker():
    """
    Session-scoped tracker to ensure in_process=True runs only once
    unless --runall-inprocess is specified.
    """
    return {'has_run_in_process': False}


@pytest.fixture(params=[False, True], ids=['in_child_process', 'in_main_process'])
def in_process(request, in_process_tracker, pytestconfig):
    value = request.param
    if not pytestconfig.getoption('--runall-inprocess') and value:
        if in_process_tracker['has_run_in_process']:
            pytest.skip("Skipping in_process=True after running once")
        else:
            in_process_tracker['has_run_in_process'] = True
    return value


@pytest.fixture(
    params=[
        pytest.param((BatchedLiveView1D, put_data_batched_1d, {'n_lines': 1}),
                     id='BatchedLiveView1D_single_line'),
        pytest.param((BatchedLiveView1D, put_data_batched_1d, {'n_lines': 2}),
                     id='BatchedLiveView1D_multiple_lines_line'),
        pytest.param((IncrementalLiveView1D, put_data_incremental_1d, {'n_lines': 1}),
                     id='IncrementalLiveView1D_one_line'),
        pytest.param((IncrementalLiveView1D, put_data_incremental_1d, {'n_lines': 3}),
                     id='IncrementalLiveView1D_three_lines'),
        pytest.param((BatchedLiveView2D, put_data_batched_2d, {}),
                     id='BatchedLiveView2D'),
        pytest.param((IncrementalLiveView2D, put_data_incremental_2d, {}),
                     id='IncrementalLiveView2D'),
    ]
)
def initialized(request, rng, in_process, in_gitlab_ci):
    cls, put_data, kwargs = request.param
    data_source = put_data(rng, **kwargs)
    view_or_proxy = get_constructor(cls, in_process, in_gitlab_ci)(data_source, **kwargs)

    yield view_or_proxy, data_source

    view_or_proxy.stop()
    if in_process:
        view_or_proxy.process.terminate()
    else:
        close_figure(view_or_proxy.fig)


@pytest.fixture
def running(initialized, in_process, rng, launch_timeout):
    view_or_proxy, data_source = initialized
    if not in_process:
        view_or_proxy.start()

        start_animation(view_or_proxy.fig)
        advance_frames(view_or_proxy, rng.integers(1, 11))
    else:
        view_or_proxy.block_until_ready()

    assert not statement_timed_out(lambda: view_or_proxy.is_running(), launch_timeout)

    yield view_or_proxy, data_source


@pytest.fixture
def stopped(running, in_process, rng):
    view_or_proxy, data_source = running
    view_or_proxy.stop()

    yield view_or_proxy, data_source


@pytest.fixture
def reattached_while_stopped(stopped, in_process, rng):
    view_or_proxy, data_source = stopped
    view_or_proxy.attach(data_source, interval=10.0 ** rng.integers(-4, 0))
    yield view_or_proxy, data_source


@pytest.fixture
def reattached_and_started(reattached_while_stopped, in_process, rng):
    view_or_proxy, data_source = reattached_while_stopped
    if not in_process:
        view_or_proxy.start()

        start_animation(view_or_proxy.fig)
        advance_frames(view_or_proxy, rng.integers(1, 11))

    yield view_or_proxy, data_source


@pytest.fixture
def reattached_while_running(running, in_process, rng):
    view_or_proxy, data_source = running
    view_or_proxy.attach(data_source, interval=10.0 ** rng.integers(-4, 0))
    if not in_process:
        advance_frames(view_or_proxy, rng.integers(1, 11))

    yield view_or_proxy, data_source


@pytest.fixture
def killed(running, in_process, rng):
    view_or_proxy, data_source = running

    if in_process:
        view_or_proxy.close_event.set()
    else:
        close_figure(view_or_proxy.fig)

    yield view_or_proxy, data_source


def test_running(running, in_process: bool):
    view_or_proxy, data_source = running

    assert view_or_proxy.is_running()
    assert view_or_proxy.data_thread.is_alive()
    assert not view_or_proxy.stop_event.is_set()
    if in_process:
        assert view_or_proxy.process.is_alive()
    else:
        # Start twice
        with pytest.raises(RuntimeError) as exc_info:
            view_or_proxy.start()

        assert 'threads can only be started once' in str(exc_info.value)


def test_pause_resume(running: tuple[LiveViewT, DataSource], in_process: bool, rng):
    if in_process:
        pytest.skip("Skipping test_pause_resume: not available in_process")

    view, data_source = running

    view.toggle_pause_resume()

    assert not view.is_running()
    assert view.data_thread.is_alive()
    assert not view.stop_event.is_set()

    view.toggle_pause_resume()

    advance_frames(view, rng.integers(1, 11))

    assert view.is_running()
    assert view.data_thread.is_alive()
    assert not view.stop_event.is_set()


def test_stopped(stopped, in_process: bool):
    view_or_proxy, data_source = stopped

    assert view_or_proxy.is_running() is None
    assert not view_or_proxy.data_thread.is_alive()
    assert view_or_proxy.stop_event.is_set()

    if not in_process:
        with pytest.raises(RuntimeError) as exc_info:
            view_or_proxy.start()

        assert 'No data source attached' in str(exc_info.value)


def test_reattached_while_stopped(reattached_while_stopped, in_process: bool, rng):
    view_or_proxy, data_source = reattached_while_stopped

    if not in_process:
        assert view_or_proxy.is_running() is None
        assert not view_or_proxy.data_thread.is_alive()
        assert not view_or_proxy.stop_event.is_set()


def test_reattached_and_started(reattached_and_started, in_process: bool, rng):
    view_or_proxy, data_source = reattached_and_started

    assert view_or_proxy.is_running()
    assert view_or_proxy.data_thread.is_alive()
    assert not view_or_proxy.stop_event.is_set()
    if in_process:
        assert view_or_proxy.process.is_alive()


def test_reattached_while_running(reattached_while_running, in_process: bool, rng):
    view_or_proxy, data_source = reattached_while_running

    assert view_or_proxy.is_running()
    assert view_or_proxy.data_thread.is_alive()
    assert not view_or_proxy.stop_event.is_set()
    if in_process:
        assert view_or_proxy.process.is_alive()


def test_killed(killed, in_process: bool, rng):
    view_or_proxy, data_source = killed

    assert not statement_timed_out(lambda: view_or_proxy.is_running() is None, timeout=1)
    assert not statement_timed_out(lambda: not view_or_proxy.data_thread.is_alive(), timeout=1)
    assert not statement_timed_out(view_or_proxy.stop_event.is_set, timeout=1)

    with pytest.raises(RuntimeError) as exc_info:
        view_or_proxy.attach(data_source)

    if in_process:
        assert 'view has been closed' in str(exc_info.value)
    else:
        assert 'figure has been closed' in str(exc_info.value)
