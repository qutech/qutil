"""Mock spectrometer import for pytest."""
import sys
from types import ModuleType
from unittest.mock import Mock

sys.modules['python_spectrometer'] = ModuleType('python_spectrometer')
sys.modules['python_spectrometer'].Spectrometer = Mock()

sys.modules['python_spectrometer.daq'] = ModuleType('python_spectrometer.daq')
